package main

import (
	"log"
	"os"
	"strings"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ec2"
	"github.com/aws/aws-sdk-go/service/ssm"
)

// Concurrency safe according to https://docs.aws.amazon.com/sdk-for-go/api/service/ssm/
var ssmSession = ssm.New(session.New(), aws.NewConfig())
var ec2Session = ec2.New(session.New(), aws.NewConfig())

func invokeSSMDocument(event events.CloudWatchEvent) {
	tagName := os.Getenv("TAG_NAME")
	tagValue := os.Getenv("TAG_VALUE")
	ssmDocument := os.Getenv("SSM_DOCUMENT_NAME")
	ssmParamKeys := os.Getenv("SSM_PARAM_KEYS")
	ssmParamVals := os.Getenv("SSM_PARAM_VALS")

	if strings.TrimSpace(tagName) == "" {
		log.Println("Missing parameter TAG_NAME")
		return
	}

	if strings.TrimSpace(tagValue) == "" {
		log.Println("Missing parameter TAG_VALUE")
		return
	}

	if strings.TrimSpace(ssmDocument) == "" {
		log.Println("Missing parameter SSM_DOCUMENT_NAME")
		return
	}

	if strings.TrimSpace(ssmParamKeys) == "" {
		log.Println("Missing parameter SSM_PARAM_KEYS")
		return
	}

	if strings.TrimSpace(ssmParamVals) == "" {
		log.Println("Missing parameter SSM_PARAM_VALS")
		return
	}

	keys := strings.Split(ssmParamKeys, ",")
	vals := strings.Split(ssmParamVals, ",")

	if len(keys) != len(vals) {
		log.Printf(
			"Number of SSM parameter keys (%d) does not match number of value (%d)",
			len(keys),
			len(vals),
		)

		return
	}

	ec2Input := &ec2.DescribeInstancesInput{
		Filters: []*ec2.Filter{
			&ec2.Filter{
				Name:   aws.String("tag:" + tagName),
				Values: []*string{aws.String(tagValue)},
			},
		},
	}

	ec2Output, ec2Err := ec2Session.DescribeInstances(ec2Input)
	if ec2Err != nil {
		log.Println("Unable to DescribeInstances.")
		logError(ec2Err)
		return
	}

	log.Print(ec2Output)

	if len(ec2Output.Reservations) < 1 {
		log.Println("No reservations found with a tag name 'Service' and a value of 'svn'")
		return
	}

	if len(ec2Output.Reservations[0].Instances) < 1 {
		log.Println("No instances found with a tag name 'Service' and a value of 'svn'")
		return
	}

	// Assume we only need to back up the first instance as all instances share the same volume
	instanceID := ec2Output.Reservations[0].Instances[0].InstanceId

	params := make(map[string][]*string)
	for i := 0; i < len(keys); i++ {
		params[keys[i]] = []*string{aws.String(vals[i])}
	}

	ssmInput := &ssm.SendCommandInput{
		DocumentName: aws.String(ssmDocument),
		InstanceIds:  []*string{instanceID},
		Parameters:   params,
	}

	ssmOutput, ssmErr := ssmSession.SendCommand(ssmInput)

	if ssmErr != nil {
		log.Println("Unable to SendCommand.")
		logError(ssmErr)
		return
	}

	log.Print(ssmOutput)
}

func logError(err error) {
	log.Print("An error happened")
	if awsErr, ok := err.(awserr.Error); ok {
		log.Println("Error:", awsErr.Code(), awsErr.Message())
	} else {
		log.Println(err.Error())
	}
}

func main() {
	lambda.Start(invokeSSMDocument)
}
