# tf-aws-scheduled-ssm-invoke

This is a work in progress module for triggering SSM documents using CloudWatch Events by way of a Golang Lambda function.

## Terraform version compatibility

| Module version | Terraform version |
|----------------|-------------------|
| 1.x.x          | 0.12.x            |
| 0.x.x          | 0.11.x            |

## Updating the invoke-ssm binary

Assuming a working Go development environment, simply execute:
```
GOOS=linux go build invoke-ssm.go
```
In an ideal world the compilation would be performed as part of a Terraform run however most people don't have a Go development environment.

## TODO
- Better build chain for Go
- Accept the params as a JSON
