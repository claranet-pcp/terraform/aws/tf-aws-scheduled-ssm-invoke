### IAM ###
data "aws_iam_policy_document" "lamda_assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "lambda_access_policy" {
  statement {
    actions = [
      "ssm:SendCommand",
      "ssm:ListCommandInvocations",
      "ec2:DescribeInstances",
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents",
    ]

    resources = [
      "*",
    ]
  }
}

resource "aws_iam_role" "lambda" {
  name               = "${var.name}-${var.envname}-lambda-role"
  assume_role_policy = data.aws_iam_policy_document.lamda_assume_role_policy.json
}

resource "aws_iam_role_policy" "cloudwatch_logs_policy" {
  name   = "${var.name}-${var.envname}-lambda-access-policy"
  role   = aws_iam_role.lambda.name
  policy = data.aws_iam_policy_document.lambda_access_policy.json
}

resource "aws_iam_policy_attachment" "attach_policies" {
  count      = length(var.iam_policy_arns)
  name       = "${var.name}-${var.envname}-policy-${count.index}"
  roles      = [aws_iam_role.lambda.name]
  policy_arn = element(var.iam_policy_arns, count.index)
}

### Lambda ###
resource "aws_lambda_function" "invoke_ssm_lambda" {
  filename         = ".terraform/invoke-ssm.zip"
  source_code_hash = data.archive_file.archive.output_base64sha256
  function_name    = "invokeSSMDocument-${var.name}"
  role             = aws_iam_role.lambda.arn
  handler          = "invoke-ssm"
  runtime          = "go1.x"
  timeout          = "300"

  environment {
    variables = {
      TAG_NAME          = var.tag_name
      TAG_VALUE         = var.tag_value
      SSM_DOCUMENT_NAME = var.ssm_document_name
      SSM_PARAM_KEYS    = var.ssm_param_keys
      SSM_PARAM_VALS    = var.ssm_param_vals
    }
  }
}

### CloudWatch ###
resource "aws_cloudwatch_event_rule" "invoke_ssm_rule" {
  name                = "${var.name}-${var.envname}-invoke-ssm"
  description         = "Trigger scheudled invocation of a SSM documet"
  schedule_expression = var.invoke_ssm_schedule
}

resource "aws_cloudwatch_event_target" "invoke_ssm_target" {
  rule = aws_cloudwatch_event_rule.invoke_ssm_rule.name
  arn  = aws_lambda_function.invoke_ssm_lambda.arn
}

resource "aws_lambda_permission" "allow_cloudwatch_to_call_invoke_ssm" {
  statement_id  = "AllowExecutionFromCloudWatchstart"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.invoke_ssm_lambda.function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.invoke_ssm_rule.arn
}
