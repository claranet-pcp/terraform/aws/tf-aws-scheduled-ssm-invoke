data "archive_file" "archive" {
  type        = "zip"
  source_file = "${path.module}/invoke-ssm"
  output_path = "${path.cwd}/.terraform/invoke-ssm.zip"
}
