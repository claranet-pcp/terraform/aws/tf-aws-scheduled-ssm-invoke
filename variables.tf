variable "name" {}

variable "envname" {}

variable "tag_name" {}

variable "tag_value" {}

variable "ssm_document_name" {}

variable "ssm_param_keys" {}

variable "ssm_param_vals" {}

variable "invoke_ssm_schedule" {}

variable "iam_policy_arns" {
  type    = list(string)
  default = []
}
